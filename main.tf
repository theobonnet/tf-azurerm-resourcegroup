locals {
  name = replace(var.project_name, " ", "")
  // Max of keyvault name is 24 and we substitute prefix and suffix length.
  // Suffix lenght is max when it staging
  keyvault_name_selector = var.keyvault_name == "" ? local.name : replace(var.keyvault_name, " ", "")
  keyvault_name = substr(local.keyvault_name_selector, 0, 24 - 3 - length(var.project_entity) - length("staging"))
  resourcegroup_name = var.resourcegroup_name != "" ? var.resourcegroup_name : azurerm_resource_group.default[0].name
  keyvault = var.keyvault_name != "" ? data.azurerm_key_vault.existing[0] : azurerm_key_vault.default[0]
}

data "azurerm_subscription" "current" {
}

data "azurerm_resource_group" "existing" {
  count = var.resourcegroup_name != "" ? 1 : 0
  name = var.resourcegroup_name
}

data "azurerm_key_vault" "existing" {
  count = var.keyvault_name != "" ? 1 : 0
  name                = var.keyvault_name
  resource_group_name = var.resourcegroup_name
}

resource "azurerm_resource_group" "default" {
  count = var.resourcegroup_name != "" ? 0 : 1
  location = var.azure_region
  name = lower("reg${var.project_entity}${local.name}${var.environment}")
  
  tags = merge (
    { 
      environment = var.environment 
    },
    var.tags
  )
}


data "azuread_service_principal" "service-connection" {
  count = var.project_application_client_id != "" ? 1 : 0
  application_id = var.project_application_client_id
}

resource "azurerm_role_assignment" "devops-contributor" {
  count = var.project_application_client_id != "" ? 1 : 0
  scope = var.resourcegroup_name != "" ? data.azurerm_resource_group.existing[0].id : azurerm_resource_group.default[0].id
  role_definition_name = "Contributor"
  principal_id = data.azuread_service_principal.service-connection[0].object_id
}


locals {
  groups = distinct(var.groups)
}

/*
resource "azurerm_role_assignment" "contributor" {
  count = length(local.groups)
  scope = azurerm_resource_group.default.id
  role_definition_name = "Contributor"
  principal_id = data.azuread_group.readonly[count.index].id
}
*/
data "azuread_group" "readonly"{
  count = length(local.groups)
  display_name = local.groups[count.index]
}


resource "azurerm_key_vault" "default" {
  count = var.keyvault_name != "" ? 0 : 1
  location = var.azure_region
  name = lower("key${var.project_entity}${local.keyvault_name}${var.environment}")
  resource_group_name = var.resourcegroup_name != "" ? data.azurerm_resource_group.existing[0].name : azurerm_resource_group.default[0].name
  tenant_id = data.azurerm_subscription.current.tenant_id
  sku_name = "standard"
  enabled_for_template_deployment = true
  
  lifecycle {
    ignore_changes = [tags]
  }
}

resource "azurerm_key_vault_access_policy" "readonly" {
    count = length(local.groups)
    key_vault_id = var.keyvault_name != "" ? data.azurerm_key_vault.existing[0].id : azurerm_key_vault.default[0].id
    tenant_id = data.azurerm_subscription.current.tenant_id
    object_id = data.azuread_group.readonly[count.index].id 
    secret_permissions = [
      "Get",
      "List",
      "Set",
      "Delete"
    ]  
}