/*
output "location" {
  value = azurerm_resource_group.default.location
}
*/

output "name" {
  value = local.resourcegroup_name
}

output "key_vault" {
  value = local.keyvault
}
